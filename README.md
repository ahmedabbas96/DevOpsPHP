# DevOps Peoject
![](Images/app.png)
![](Images/Kapture_2024-01-02_at_09.30.14.gif)

## Describtion
    DevOps Project to deploy PHP Larvel web application with Nginx as web server and mysql as Database.
    Each component is deployed by helm. 
    Gitlab is used for CI/CD managing all docker images and store them in container registry 
    also packaging helm charts and deploy it to kubernates cluter.
    All Charts has HPA configured to ensure high availablity 

## Prerequisite
    - Kubernates cluster with gitlab agent running on it 
    - Helm
    - Gitlab

## Installation
    - how to install gitlab agent https://docs.gitlab.com/ee/user/clusters/agent/install/ 
    - how to install helm in your local so you can debug https://helm.sh/docs/intro/install/ 
    - Gitlab deploy all application components.
## Repo Structure 

```
├── Backend                                                                             
│   ├── DockerFile                                                                  
│   ├── README.md
│   ├── app
│   ├── artisan
│   ├── bootstrap
│   ├── composer.json
│   ├── composer.lock
│   ├── config
│   ├── database
│   ├── package.json
│   ├── phpunit.xml
│   ├── public
│   ├── resources
│   ├── routes
│   ├── storage
│   ├── tests
│   ├── vendor
│   └── vite.config.js
├── Charts
│   ├── be-larvel
│   └── fe-nginx
├── Frontend
│   ├── DockerFile
│   └── ui
├── Images
│   └── Kapture_2024-01-02_at_09.30.14.gif
└── README.md
```

- Backend directory has all larvel code and dockerfile.
- Charts directory has all helm charts execpt mysql chart is deployed by bitnami chart.
- Frontebd directory has all frontend files and dockerfile.


## Tasks

    Task1: Create the UI and dockerize it and package it with helm
    Task2: Create Gitlab pipeline to deploy UI to Kubernates and test it 
    Task3: Deploy MySQL as Helmchart using Gitlab
    Task4: Develop locally Larvel api to get and post to MySQL Database
    Task5: Dockerize Larvel api and package as helmchart to be deployed
    Task6: Create pipeline to deplou Larvel api to k8s and test it
    Task7: Test the full flow is working and fix bugs

## Pipline Diagram

![](Images/pipeline.png)

## Testing & Issues

    UI is tested in pipeline :)
    Database is working :)
    Larvel Api is working locally but when i deploy helm chart the pod craches :(

## Enhancment

    Deploy muliple instance of mysql database
    Deploy Redis instance to minimize the response time 
    Deploy Oservability tools to monitor all the components
    Store passowrds in secret store
