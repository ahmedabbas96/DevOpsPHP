<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MessageControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
     /**
     * Test inserting data.
     *
     * @return void
     */
    public function testInsertData()
    {
        $response = $this->json('POST', 'api/insert-data', [
            'firstname' => 'test1',
            'lastname' => 'test2',
            'email' => 'test@test.com',
            'message' => 'this is a test message'
        ]);

        $response->assertStatus(200)
            ->assertJson(['message' => 'Data inserted successfully']);
    }
    /**
     * Test retrieving data.
     *
     * @return void
     */
    public function testGetData()
    {
        // Assuming there is data in the database
        $response = $this->json('GET', 'api/get-data');

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'fristname',
                    'lastname',
                    'email',
                    'message',
                ]
            ]);
    }
}
