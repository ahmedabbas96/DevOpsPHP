<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;

class MessageController extends Controller
{
    public function insertData(Request $request)
    {
        // Create a new record in the database
        Message::create([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'message' => $request->input('message'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return response()->json(['message' => 'Data inserted successfully']);
    }

    public function getData()
    {
        // Retrieve all records from the database
        $data = Message::all();

        return response()->json($data);
    }
}
